const express=require('express');
const router=express.Router();
const paypal=require('paypal-rest-sdk');
const { Store }= require('./modul/store');
const path = require('path');
const s=new Store();


paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': 'AYJbUnccPFL4ks8qNeifUsqswjRA93aNJ77K3cbAp-P0bqkaZ8oshbL2xU1sqizmwiUwje2TSoBYk9-V',
    'client_secret': 'ELPNSvkIf6IO9EkfERphChOn2xjYuSsL0NVD6d8icc4KL6ybrhGI2U3Xm4IVFP7rwkeUTYjcMeSJFKCz'
  });

router.post('/pay', (req,res)=>{
    
    console.log(`${req.body.obiekt.nazwa}, ${req.body.obiekt.cena}, ${req.body.obiekt.transakcjaId}`)
   
    var create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "http://localhost:3000/api/success",
            "cancel_url": "http://localhost:3000/api/cancel"
        },
        "transactions": [{
            "item_list": {
                "items": [{
                    "name": `FindPrinter - Drukowanie  - ${req.body.obiekt.nazwa}`,
                    "sku": "0001",
                    "price": req.body.obiekt.cena,
                    "currency": "PLN",
                    "quantity": req.body.obiekt.ilosc
                }]
            },
            "amount": {
                "currency": "PLN",
                "total": (req.body.obiekt.cena*req.body.obiekt.ilosc)
            },
            "description": `Usluga drukowania za posrednictwem ${req.body.obiekt.nazwa}`
        }]
    };
    paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
    
            console.log(error);
            console.log(error.response.details);
        } else {
            let liczba=payment.links[1].href.indexOf("token");
            liczba+=6;
            let ob=payment.links[1].href;
            let tok=ob.substring(liczba,ob.length);
            console.log("Tok:");
            console.log(tok);

            let obiekto={
                klucz: payment.id,
                nazwa: req.body.obiekt.nazwa,
                cena: req.body.obiekt.cena,
                token: req.headers.authorization,
                transakcjaId: req.body.obiekt.transakcjaId,
                ilosc: req.body.obiekt.ilosc,
                cancelTok: tok
            }
            
           s.add(obiekto);
            console.log("Payment");
            console.log(payment);
            console.log('Poszlo');
            for(let n=0;n<payment.links.length;n++){
              if(payment.links[n].rel=='approval_url'){
                  let obiekt={
                      url: payment.links[n].href
                  }
                res.json(obiekt);
              }
            }
        }
    });
});
router.get('/success',(req,res)=>{
    console.log("Poszedl sukces");
    //console.log(s.get('dupa'));
    
    const PayerId=req.query.PayerID;
    const paymentId=req.query.paymentId;
    let ob=s.get(paymentId);
    console.log("Obiekt cena: ");
    console.log(ob.cena);
    console.log("Obiekt ilosc: ");
    console.log(ob.ilosc);
    const execute_payment_json = {
        "payer_id": PayerId,
        "transactions": [{
            "amount": {
                "currency": "PLN",
                "total": (parseFloat(ob.cena)*parseFloat(ob.ilosc))
            }
        }]
      };
      console.log("Obiekt payment");
      console.log(execute_payment_json);
      console.log(execute_payment_json.transactions[0].amount.total);
      paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
            console.log(error.response);
            throw error;
        } else {
            console.log(payment);
            let patho=path.resolve('./routes/pages/done.html');
            res.sendFile(patho);
            s.wykonaj(payment.id)
        }
      });

});
router.get('/cancel',(req,res)=>{
        const token=req.query.token;
        let patho=path.resolve('./routes/pages/analuj.html');
        res.sendFile(patho);
        s.wykonajAnuluj(token);

});

module.exports=router;