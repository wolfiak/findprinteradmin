const express=require('express');
const path=require('path');
const socket=require('socket.io');
const bodyParser = require('body-parser');
var cors = require('cors')
const api=require('./routes/api');
const session = require('express-session');

const app=express();
app.use(cors({credentials: true, origin: true}));

    app.use(express.static(path.join(__dirname,'dist')));
    app.use('/pages',express.static(path.join(__dirname, 'routes/pages')));



app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
  })); 
  
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(session({ secret: 'piano cat',
  resave: false,
  saveUninitialized: true
}));

app.use('/api',api);

app.get('*',(req,res)=>{
    res.sendFile(path.join(__dirname,'dist'));
});

const port=process.env.PORT || '3000';
app.set('port',port);

const server=app.listen(port,()=>{
    console.log(`Serwer wystartowal na porcie: ${port}`);
});


const io=socket(server);

io.on('connection',(sock)=>{

    sock.on('subscribe',(room)=>{
        console.log(`Dolaczenie do pokoju: `,room);
        sock.join(room);
    });
    sock.on('send',(data)=>{
        console.log(`room post ${data.room} : ${data.msg} xero: ${data.xero}`);
        sock.broadcast.in(data.room).emit('post',{
            msg: data.msg,
            xero:data.xero
        });
    });
});
