import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { LogowanieComponent } from './component/logowanie/logowanie.component';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material';
import {MatCardModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';

import { ModalService } from './service/modal.service';
import {LogowanieService} from './service/logowanie.service';
import {WezDrukarkeService} from './service/wez-drukarke.service';
import {TransakcjeService} from './service/transakcje.service';

import {CzatService} from './service/czat.service';
import { NawigacjaComponent } from './component/nawigacja/nawigacja.component';
import { TransakcjeComponent } from './component/transakcje/transakcje.component';
import { WiadomosciComponent } from './component/wiadomosci/wiadomosci.component';
//import {DragulaModule } from 'dragula';
import { DragulaService, DragulaModule } from 'ng2-dragula/ng2-dragula';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent, children:[
    { path: 'trans', component: TransakcjeComponent, outlet:'aux'},
    { path: 'wiad',component: WiadomosciComponent, outlet:'aux'}
    
  ] },
  { path: '', redirectTo:'/home', pathMatch: 'full'},
  { path: 'logo', component: LogowanieComponent },
  
 
  
];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LogowanieComponent,
    NawigacjaComponent,
    TransakcjeComponent,
    WiadomosciComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    DragulaModule,
    
    RouterModule.forRoot(
      appRoutes
  )
  ],
  providers: [LogowanieService, WezDrukarkeService, TransakcjeService, ModalService, CzatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
