import { Component, OnInit } from '@angular/core';
import { IUzytkownik } from './../../model/IUzytkownik';
import { LogowanieService } from "./../../service/logowanie.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-logowanie',
  templateUrl: './logowanie.component.html',
  styleUrls: ['./logowanie.component.css']
})
export class LogowanieComponent implements OnInit {
   private login: string;
   private pass:string;
  constructor(private ls: LogowanieService, private router: Router) { }

  ngOnInit() {
  }
  
  logowanie() {
    let uz = {
        imie: '',
        nazwisko: '',
        email: this.login,
        password: this.pass
    };
    this.ls.loguj(uz).subscribe(
        res => {
            if (res) {
                this.router.navigate(['']);
            }
        },
        err => console.log(err)

    );
}
}
