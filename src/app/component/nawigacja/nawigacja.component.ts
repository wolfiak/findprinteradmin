import { Component, OnInit } from '@angular/core';
import { WezDrukarkeService } from "./../../service/wez-drukarke.service";
import {Router} from '@angular/router';
@Component({
  selector: 'app-nawigacja',
  templateUrl: './nawigacja.component.html',
  styleUrls: ['./nawigacja.component.css']
})
export class NawigacjaComponent implements OnInit {

  constructor(private wds: WezDrukarkeService, private router: Router) { }
    private avatar:string;
  ngOnInit() {
    this.ustawAvatar();
  }
  ustawAvatar(){
    console.log("test: ");
    console.log(this.wds.getWyborZTablica());
    this.avatar=this.wds.getWyborZTablica().avatar;
  }
  wiadomosci(){
    this.router.navigateByUrl('/home/(aux:wiad)');
  }
  transakcja(){
    this.router.navigateByUrl('/home/(aux:trans)');
  }
  powrotDoModala(){
    
  }
}
