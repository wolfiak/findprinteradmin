import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogowanieService } from "./../../service/logowanie.service";
import { WezDrukarkeService } from "./../../service/wez-drukarke.service";
import { ModalService } from "./../../service/modal.service";
import { TransakcjeService } from "./../../service/transakcje.service";
import { CzatService } from "./../../service/czat.service";
import 'rxjs/Rx' ;
import * as FileSaver from 'file-saver';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private modal: boolean = true;
  private tablicaDoWyboru: any[];
  private wybor: string;
  private modal2: boolean =false;
  private tmpUz: any;
  constructor(private ls: LogowanieService, private router: Router, 
    private wds: WezDrukarkeService, private ms: ModalService, private ts: TransakcjeService
    ,private czs: CzatService ) { }

  ngOnInit() {
    this.zalogowany().then(
      res=>{
        this.transakcje();
        this.przedWyborXero();
        this.ms.getModal2().subscribe(res=>{
          console.log("Respond z modala");
          console.log(res);
         // this.modal2=res;
          if(res){
            console.log("Uzytkownik id");
            console.log(this.ms.getTmpDoModal().uzytkownikIdo);
            this.ls.getUz(this.ms.getTmpDoModal().uzytkownikIdo)
              .subscribe(respond =>{
                this.tmpUz=respond;
                this.modal2=res;
              });
          }else{
            this.modal2=res;
          }
          
        });
      }
    ).catch(
      res=>{
        console.log(`Nie ma tokenu ${res}`);
      }
    )

  }

  transakcje(){
    this.router.navigateByUrl('/home/(aux:trans)');
  }
  zalogowany(){
    return new Promise((res,rej)=>{
    this.ls.isLoggedIn().subscribe(
      reso=> {
        console.log("Przyszedl zwrot");
        console.log(reso);
        res();
      },
      err=>{
        console.log("Przyszedl error");
        console.log(err);
        this.router.navigate(['logo']);
        rej();
      } 
    );
  });
  }
  przedWyborXero(){
    if(this.modal){
      this.wds.wezUzytkownika(JSON.parse(localStorage.getItem('auth_token')).id).subscribe(
        res=>{
          console.log("Wynik xero dla danego uzytkownika:");
          console.log(res);
          this.tablicaDoWyboru=res;
        }
      )
    }
  }
  wyborXero(id){
    this.modal=false;
    this.wybor=id;
    this.wds.setWybor(this.wybor);
  }
  zamknijModal(){
    this.ms.setModal2();
  }
  wezPlik(){
    this.ts.getFile(this.ms.getTmpDoModal().sciezka).subscribe(
      res=>{
        console.log("Wynik w home");
        console.log(res);
        FileSaver.saveAs(res,"test.pdf");
        let url=window.URL.createObjectURL(res);
        window.open(url);
      }
    )
  }
  czat(){
    console.log("tMP DO modala");
    console.log(this.ms.getTmpDoModal())
    this.czs.nowaRozmowa(this.ms.getTmpDoModal().xeroIdo,this.ms.getTmpDoModal().uzytkownikIdo)
      .subscribe(res=>{
       
      });
  }

}
