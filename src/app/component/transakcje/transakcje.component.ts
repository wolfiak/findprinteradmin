import { Component, OnInit } from '@angular/core';
import { TransakcjeService } from "./../../service/transakcje.service";
import { WezDrukarkeService } from "./../../service/wez-drukarke.service";
import { ModalService } from "./../../service/modal.service";
import { DragulaService, DragulaModule } from 'ng2-dragula/ng2-dragula';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-transakcje',
  templateUrl: './transakcje.component.html',
  styleUrls: ['./transakcje.component.css']
})
export class TransakcjeComponent implements OnInit {
  private tablicaPrzychodzace: any[]=[];
  private tablicaPrzychodzaceT: any[]=[];
  private tablicaObecne: any[]=[];
  private tablicaZrobine: any[]=[];
  private tmpDoModala: any;
  private filtrChange: Subject<string> =new Subject<string>();
  private filtrOChange: Subject<string> =new Subject<string>();
  private filtrZChange: Subject<string> =new Subject<string>();
  private filtr: string;
  private filtrO: string;
  private filtrZ: string;
  constructor(private ts: TransakcjeService, private wds: WezDrukarkeService
    , private ds: DragulaService, private ms: ModalService) { }

  ngOnInit() {
    this.transakcje();
    this.filtrChange.subscribe((res)=>{
      this.filtrujPrzychodzace(res);
    });
    this.filtrOChange.subscribe((res)=>{
      this.filtrujObecne(res);
    });
    this.filtrZChange.subscribe((res)=>{
      this.filtrujZrobione(res);
    });
    this.ds.drop.subscribe(d=>{
      console.log("Wykryto drop");
      console.log(d);
      this.obslugaDrop(d);
    });
  }
  obslugaDrop(drop){
    console.log("zmienam status na :");
    console.log(drop[2].classList[1]);
    console.log(drop[1].id);
    this.ts.zmianaStatusu(drop[2].classList[1],this.wds.getWybor(),drop[1].id)
      .subscribe(res=>{
        console.log("Respond");
        console.log(res);
      });
  }
  sprPlatnosc(status){
    if(status === "Oplacono"){
      return true;
    }else{
      return false;
    }
  }
  transakcje(){
    this.ts.wezTransakjce(this.wds.getWybor()).subscribe(res=>{
      console.log('Wynik w transakcja component: ');
      console.log(res);
    //  res.miejsce="przychodzaca";
      console.log('Wynik service w component');
      console.log(this.ts.getTransakcje());
      this.sortowanie(this.ts.getTransakcje());
    });

  }
  sortowanie(tablica){
    console.log("Tablic w sortowaniu");
    for(let n=0;n<tablica.length;n++){
      tablica[n].data=new Date(tablica[n].data);
      console.log("Status tablicy:");
      console.log(tablica[n].status);
      if(!tablica[n].status || tablica[n].status=="przychodzace"){
        console.log("Przychodzace");
        this.tablicaPrzychodzace.push(tablica[n]);
        this.tablicaPrzychodzaceT.push(tablica[n]);
      }else if(tablica[n].status=="obecne"){
        console.log("obecne");
        this.tablicaObecne.push(tablica[n]);
      }else if(tablica[n].status=="zrobione"){
        console.log("obecne");
        this.tablicaZrobine.push(tablica[n]);
      }
    }
   
  }

  async klikKarta(event){
    
    console.log("Karta kliknieta");
    console.log(event);
    let elem= await this.znajdzElement(event);
    console.log("Konkretyny element: ");
    console.log(elem);
    await this.konkretnyElement(elem);
    this.ms.setModal2();
  }
  async konkretnyElement(element){
    console.log("Badanie elementu: ");
    console.log(element);
    let index=element.indexOf(".");
    let tablica=element.substring(0,index);
    let id=element.substring(index+1,element.length);
    console.log(tablica);
    if(tablica == "obecne"){
        return this.szukajWtablicach(this.tablicaObecne,id);
    }else if(tablica == "przychodzace"){
      return this.szukajWtablicach(this.tablicaPrzychodzace,id);
    }else if(tablica == "zrobione"){
      return this.szukajWtablicach(this.tablicaZrobine,id);
    }
  }
  szukajWtablicach(tablica,id){
    for(let n=0;n<tablica.length;n++){
      if(tablica[n].id == id){
        this.ms.setTmpDoModala(tablica[n]);
        return tablica[n];
      }
    }
  }
   znajdzElement(event){
     console.log("znajdz element");
     console.log(event);
    for(let n=0;n<event.path.length;n++){
      if(event.path[n].classList[0]=="kartaT"){
        return event.path[n].id;
      }
    }

  }
  onTap(value: string){
    this.filtrChange.next(value);
  }
  onTapO(value: string){
    this.filtrOChange.next(value);
  }
  onTapZ(value: string){
    this.filtrZChange.next(value);
  }
  filtrujPrzychodzace(res){
    console.log(res);
    if(res.length == 0){
      console.log("res length 0");
     // this.tablicaPrzychodzace=[];
      for(let n=0;n<this.tablicaPrzychodzace.length;n++){
       // this.tablicaPrzychodzace.push(this.tablicaPrzychodzaceT[n]);
       /* let element=this.tablicaPrzychodzace[n];
        console.log(element);
        let tekst= document.getElementById(`nazwaP.${element.id}`);
        console.log(tekst);
        tekst.innerHTML=element.nazwa;
        */
        let element=this.tablicaPrzychodzace[n];
        let dok=document.getElementById(`przychodzace.${this.tablicaPrzychodzace[n].id}`);
        dok.className="kartaT mat-card";
        document.getElementById(`cenaP.${element.id}`).innerHTML=element.cena;
        document.getElementById(`nazwaP.${element.id}`).innerHTML=element.nazwa;
        document.getElementById(`piorytetP.${element.id}`).innerHTML=element.piorytet;
      }
      
    }else{
    
    for(let n=0;n<this.tablicaPrzychodzace.length;n++){
      let element=this.tablicaPrzychodzace[n];
      console.log(this.tablicaPrzychodzace[n]);
      let index=element.nazwa.indexOf(res);
      let cena=element.cena;
      let piorytet=element.piorytet.indexOf(res);
      if(index != -1){
        let tekst= document.getElementById(`nazwaP.${element.id}`);
        let sam=element.nazwa.replace(res,`<span  style="background: yellow;">${res}</span>`);
        tekst.innerHTML=sam;
      }else if(cena == res){
        console.log("zaszla cena:");
        let tekst= document.getElementById(`cenaP.${element.id}`);
        let sam=`<span  style="background: yellow;">${res}</span>`;
        tekst.innerHTML=sam;
      } else if(piorytet != -1){
        let dok=document.getElementById(`przychodzace.${this.tablicaPrzychodzace[n].id}`);
        dok.className="kartaT mat-card";
        console.log("Zachodzi w piorytetach res: "+res);
        let tekst= document.getElementById(`piorytetP.${element.id}`);
        let sam=element.piorytet.replace(res,`<span  style="background: yellow;">${res}</span>`);
        tekst.innerHTML=sam;
        
      }else{
      //  this.tablicaPrzychodzace.splice(n);
      let dok=document.getElementById(`przychodzace.${this.tablicaPrzychodzace[n].id}`);
      dok.className +=" hide";
      }
    }
  }
  }
  filtrujObecne(res){
    console.log(res);
    if(res.length == 0){
      console.log("res length 0");
     // this.tablicaPrzychodzace=[];
      for(let n=0;n<this.tablicaObecne.length;n++){
       // this.tablicaPrzychodzace.push(this.tablicaPrzychodzaceT[n]);
       /* let element=this.tablicaPrzychodzace[n];
        console.log(element);
        let tekst= document.getElementById(`nazwaP.${element.id}`);
        console.log(tekst);
        tekst.innerHTML=element.nazwa;
        */
        let element=this.tablicaObecne[n];
        let dok=document.getElementById(`obecne.${this.tablicaObecne[n].id}`);
        dok.className="kartaT mat-card";
        document.getElementById(`cenaO.${element.id}`).innerHTML=element.cena;
        document.getElementById(`nazwaO.${element.id}`).innerHTML=element.nazwa;
        document.getElementById(`piorytetO.${element.id}`).innerHTML=element.piorytet;
      }
      
    }else{
    
    for(let n=0;n<this.tablicaObecne.length;n++){
      let element=this.tablicaObecne[n];
      console.log(this.tablicaObecne[n]);
      let index=element.nazwa.indexOf(res);
      let cena=element.cena;
      let piorytet=element.piorytet.indexOf(res);
      if(index != -1){
        let tekst= document.getElementById(`nazwaO.${element.id}`);
        let sam=element.nazwa.replace(res,`<span  style="background: yellow;">${res}</span>`);
        tekst.innerHTML=sam;
      }else if(cena == res){
        console.log("zaszla cena:");
        let tekst= document.getElementById(`cenaO.${element.id}`);
        let sam=`<span  style="background: yellow;">${res}</span>`;
        tekst.innerHTML=sam;
      } else if(piorytet != -1){
        let dok=document.getElementById(`obecne.${this.tablicaObecne[n].id}`);
        dok.className="kartaT mat-card";
        console.log("Zachodzi w piorytetach res: "+res);
        let tekst= document.getElementById(`piorytetO.${element.id}`);
        let sam=element.piorytet.replace(res,`<span  style="background: yellow;">${res}</span>`);
        tekst.innerHTML=sam;
        
      }else{
      //  this.tablicaPrzychodzace.splice(n);
      let dok=document.getElementById(`obecne.${this.tablicaObecne[n].id}`);
      dok.className +=" hide";
      }
    }
  }
  }
  filtrujZrobione(res){
    console.log(res);
    if(res.length == 0){
      console.log("res length 0");
     // this.tablicaPrzychodzace=[];
      for(let n=0;n<this.tablicaZrobine.length;n++){
       // this.tablicaPrzychodzace.push(this.tablicaPrzychodzaceT[n]);
       /* let element=this.tablicaPrzychodzace[n];
        console.log(element);
        let tekst= document.getElementById(`nazwaP.${element.id}`);
        console.log(tekst);
        tekst.innerHTML=element.nazwa;
        */
        let element=this.tablicaZrobine[n];
        let dok=document.getElementById(`zrobione.${element.id}`);
        dok.className="kartaT mat-card";
        document.getElementById(`cenaZ.${element.id}`).innerHTML=element.cena;
        document.getElementById(`nazwaZ.${element.id}`).innerHTML=element.nazwa;
        document.getElementById(`piorytetZ.${element.id}`).innerHTML=element.piorytet;
      }
      
    }else{
    
    for(let n=0;n<this.tablicaZrobine.length;n++){
      let element=this.tablicaZrobine[n];
      console.log(this.tablicaZrobine[n]);
      let index=element.nazwa.indexOf(res);
      let cena=element.cena;
      let piorytet=element.piorytet.indexOf(res);
      if(index != -1){
        let tekst= document.getElementById(`nazwaZ.${element.id}`);
        let sam=element.nazwa.replace(res,`<span  style="background: yellow;">${res}</span>`);
        tekst.innerHTML=sam;
      }else if(cena == res){
        console.log("zaszla cena:");
        let tekst= document.getElementById(`cenaZ.${element.id}`);
        let sam=`<span  style="background: yellow;">${res}</span>`;
        tekst.innerHTML=sam;
      } else if(piorytet != -1){
        let dok=document.getElementById(`zrobione.${this.tablicaZrobine[n].id}`);
        dok.className="kartaT mat-card";
        console.log("Zachodzi w piorytetach res: "+res);
        let tekst= document.getElementById(`piorytetZ.${element.id}`);
        let sam=element.piorytet.replace(res,`<span  style="background: yellow;">${res}</span>`);
        tekst.innerHTML=sam;
        
      }else{
      //  this.tablicaPrzychodzace.splice(n);
      let dok=document.getElementById(`zrobione.${this.tablicaZrobine[n].id}`);
      dok.className +=" hide";
      }
    }
  }
  }


}
