import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransakcjeComponent } from './transakcje.component';

describe('TransakcjeComponent', () => {
  let component: TransakcjeComponent;
  let fixture: ComponentFixture<TransakcjeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransakcjeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransakcjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
