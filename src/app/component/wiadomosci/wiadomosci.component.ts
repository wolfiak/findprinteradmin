import { Component, OnInit, HostListener } from '@angular/core';
import { CzatService } from "./../../service/czat.service";
import { WezDrukarkeService } from "./../../service/wez-drukarke.service";
import * as io from "socket.io-client";
@Component({
  selector: 'app-wiadomosci',
  templateUrl: './wiadomosci.component.html',
  styleUrls: ['./wiadomosci.component.css']
})
export class WiadomosciComponent implements OnInit {
  private socket: any;
  private wiadomosc: string;
  private wiadomosci: any;
  private kontakty: any[];
  private port :number;
  private xero: boolean=true;
  private index: string;
  private tablica: boolean[]=[];
  private ukryawj: boolean=false;
  private pokaKontakt: boolean=false;
  private xeroId: string;
  constructor(private czs: CzatService, private wds: WezDrukarkeService) { }

  ngOnInit() {
    //this.socketInit();
    console.log("Wez drukarke service modal:");
    console.log(this.wds.getWybor());
    this.czs.initCzat(this.wds.getWybor())
      .subscribe(
        res=>{
          console.log(`Wynik w wiadomosciacj `);
          console.log(res);
          this.kontakty=res;
          this.tablica=[];
          for(let n=0;n<res.length;n++){
            this.tablica[n]=false;
          }
        }
      );
  }
  socketInit(){

    this.port = this.kontakty[this.index].Xero.ID;
    
    this.socket=io('http://localhost:3000');
    this.socket.emit('subscribe',this.port);
    this.wiadomosci=document.querySelector('#wiadomosci');
    this.socket.on('post',(res)=>{
      console.log("Lapie wiadomosc ");
      console.log(res);
      if(res.xero){
        console.log("Zaszlo true");
        this.wiadomosci.innerHTML += `
        <div class="wrapMoja">
        <div class="mojaWiadomosc">
          <p >${res.msg}</p>
        </div>
        </div>
         `;
      }else{
        console.log("Zaszedl  else");
        this.wiadomosci.innerHTML += `  
        <div class="content">
        <div class="zObrazkeim">
        <img class="rounded-circle avatarWiadomosci" src="http://78.media.tumblr.com/764a7a5b55c400887a366fc95327345a/tumblr_n2b3bzPfgI1r4xjo2o1_250.gif"/>  
        </div>
       
        <div class="wiadomoscPrzychodzaca">
         <p style="margin-top: 10px;">${res.msg}</p>
        </div>
        </div>
         `;
      }
      let okno=document.querySelector('.okno');
      okno.scrollTop=okno.scrollHeight;
    });

  }
  sendSocket(){
    console.log(this.kontakty[this.index]);
    let wiad={
      Xero: this.xero,
      Tresc: this.wiadomosc,
      RozmowaID: this.kontakty[this.index].Rozmowa.ID
    }
    this.czs.wyslijWiadomosc(wiad).subscribe(
      res=>{

      }
    );
    this.wiadomosci.innerHTML += `
    <div class="wrapMoja">
    <div class="mojaWiadomosc">
      <p >${this.wiadomosc}</p>
    </div>
    </div>
     `;

     let okno=document.querySelector('.okno');
     okno.scrollTop=okno.scrollHeight;

    this.socket.emit('send',{
      room: this.port,
      msg: this.wiadomosc,
      xero: this.xero
    })
  }
  oznacz(){
    for(let n=0;n<this.tablica.length;n++){
      this.tablica[n]=false;
    }
    this.tablica[this.index]=true;
  }
  rozpoczecieRozmowy(event){
    document.getElementById("wiadomosci").innerHTML = "";
    console.log("Rozpoczecie rozmowy: ");
    console.log(event);
    this.index = event.target.id;
    let tmp = this.index.indexOf(".");
    tmp++;
    this.xeroId = this.index.substring(tmp, this.index.length);
    tmp--;
    console.log("Index przed substring: " + this.index);
    this.index = this.index.substring(0, tmp);
    this.oznacz();
    this.socketInit();
    this.czs.getWiadomosci(this.kontakty[this.index].Rozmowa.ID)
      .subscribe(
        res=>{
          this.sortowanie(res);
        }
      );
  }
  sortowanie(tab){
    console.log("Obiekt sortowania: ");
    console.log(tab);
    for(let n=0;n<tab.length;n++){
      if(tab[n].xero){
        this.wiadomosci.innerHTML += `
        <div class="wrapMoja">
        <div class="mojaWiadomosc">
          <p >${tab[n].tresc}</p>
        </div>
        </div>
         `;
      
      }else{
        this.wiadomosci.innerHTML += `  
        <div class="content">
        <div class="zObrazkeim" >
        <img class="rounded-circle avatarWiadomosci" src="http://78.media.tumblr.com/764a7a5b55c400887a366fc95327345a/tumblr_n2b3bzPfgI1r4xjo2o1_250.gif"/>  
        </div>
        <div class="wiadomoscPrzychodzaca">
         <p style="margin-top: 10px;">${tab[n].tresc}</p>
        </div>
        </div>
         `;
        
      }
    }
    let okno=document.querySelector('.okno');
    okno.scrollTop=okno.scrollHeight;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth <= 650) {
      this.ukryawj = true;
      document.getElementById('lewa').className = "znika";
    } else {
      this.ukryawj = false;
      document.getElementById('lewa').className = "";
      document.getElementById('lewa').className = "pojawia";
    }
    //this.pokaKontakty();
  }


  pokaKontakty() {
    console.log(`Ukrywaj: ${this.ukryawj}`);
    console.log(`Poka kontakty: ${this.pokaKontakt}`);
    if (!this.pokaKontakt) {
      document.getElementById('prawa').className = "pojawia";
      if (this.ukryawj) {
        document.getElementById('lewa').className = "znika";
      }
      
      this.pokaKontakt = true;
    } else {
      document.getElementById('prawa').className = "znika";
      if (this.ukryawj) {
        document.getElementById('lewa').className = "pojawia";
      }
      
      this.pokaKontakt = false;
    }
   
  }


}
