import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions,ResponseContentType } from '@angular/http';
import { environment } from './../../environments/environment';

@Injectable()
export class CzatService {
  private URL: string = environment.URL + 'api/rozmowa/nowaRozmowa';
  private URL2: string = environment.URL + 'api/rozmowa/mojeRozmowyXero';
  private URL3: string = environment.URL + 'api/rozmowa/nowaWiadomosc';
  private URL4: string = environment.URL + 'api/rozmowa/getWiadomoscDlaXero';
  constructor(private http: Http) { }

  nowaRozmowa(xeroId,uzytkownikId){
    let obiekt={
      UzytkownikID: uzytkownikId,
      XeroID: xeroId
      
    }
    let headers = new Headers();
    let token = JSON.parse(localStorage.getItem('auth_token'));
    headers.append('Content-Type', "application/json");
    headers.append('Authorization', token.token);

    return this.http.post(this.URL,obiekt,{headers})
      .map(res =>{
        console.log("Odpowiedz od serwera:");
        console.log(res);

        return true;
      });
  }

  initCzat(xeroID){
    let headers = new Headers();
    let token = JSON.parse(localStorage.getItem('auth_token'));
    headers.append('Content-Type', "application/json");
    headers.append('Authorization', token.token);

    return this.http.get(this.URL2+`?xeroID=${xeroID}`,{ headers })
      .map(res=>{
          console.log("Wynik zapytania");
          console.log(res);
          return res.json();
      })

  }

  wyslijWiadomosc(wiad){
    let headers = new Headers();
    let token = JSON.parse(localStorage.getItem('auth_token'));
    headers.append('Content-Type', "application/json");
    headers.append('Authorization', token.token);

      return this.http.post(this.URL3,wiad,{ headers })
        .map(
          res=>{
            console.log("Zwrot wyslijWiadomosc");
            console.log(wiad);
            return true;
        });

  }
  getWiadomosci(id){
    console.log(`Pobieram wiadomosc ${id} `)
    let headers = new Headers();
    let token = JSON.parse(localStorage.getItem('auth_token'));
    headers.append('Content-Type', "application/json");
    headers.append('Authorization', token.token);

    return this.http.get(this.URL4+`?id=${id}`,{headers} )
      .map(res=>{

        console.log('Zwrot z getWiadomosci');
        console.log(res);
        return res.json();
      });
  }
}
