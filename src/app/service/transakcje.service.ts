
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions,ResponseContentType } from '@angular/http';
import { environment } from './../../environments/environment';


@Injectable()
export class TransakcjeService {
  private URL: string = environment.URL + 'api/transakcje/wezTransakcje';
  private URL2: string = environment.URL + 'api/transakcje/zmienStatus';
  private URL3: string = environment.URL + 'api/transakcje/getFile';
  private transakcje: any[]=[];
  constructor(private http: Http) { }

  wezTransakjce(id){
    let headers = new Headers();
    let token = JSON.parse(localStorage.getItem('auth_token'));
    headers.append('Content-Type', "application/json");
    headers.append('Authorization', token.token);

    return this.http.post(this.URL,id,{headers})
      .map(res =>{
        this.transakcje=[];
        console.log("Wynik w wezTransakcje: ");
        console.log(res.json().transakcje);
        this.transakcje=res.json().transakcje;
        return true;
      });

  }
  wydbobywnieId(element){
    let index=element.indexOf(".");
    return element.substring(index+1,element.length);
  }
  zmianaStatusu(status,id,transId){
    let pakiet={
      Id: id,
      Status: status,
      TransakcjaId: this.wydbobywnieId(transId)
    }
    let headers = new Headers();
    let token = JSON.parse(localStorage.getItem('auth_token'));
    headers.append('Content-Type', "application/json");
    headers.append('Authorization', token.token);

    return this.http.post(this.URL2,JSON.stringify(pakiet),{headers})
      .map(res =>{
       
        return true;
      });
  }
  getTransakcje(){
    return this.transakcje;
  }
  getFile(path){
    let headers = new Headers();
    headers.append('Content-Type', "application/json");
   return this.http.get(this.URL3+`?path=${path}`, { responseType: ResponseContentType.Blob })
      .map(res=>{
        console.log("Zwrot z serwera: ");

        return new Blob([res.blob()],{type: 'application/pdf'});
      });
  }

}
