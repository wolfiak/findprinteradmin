import { TestBed, inject } from '@angular/core/testing';

import { CzatService } from './czat.service';

describe('CzatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CzatService]
    });
  });

  it('should be created', inject([CzatService], (service: CzatService) => {
    expect(service).toBeTruthy();
  }));
});
