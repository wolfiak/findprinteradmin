import { TestBed, inject } from '@angular/core/testing';

import { WezDrukarkeService } from './wez-drukarke.service';

describe('WezDrukarkeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WezDrukarkeService]
    });
  });

  it('should be created', inject([WezDrukarkeService], (service: WezDrukarkeService) => {
    expect(service).toBeTruthy();
  }));
});
