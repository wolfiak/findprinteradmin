import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class WezDrukarkeService {
  private URL: string = environment.URL + 'api/xero/xerouz';
  private wybor: string;
  private tablicaWybor: any[];
  constructor(private http: Http) { }

  wezUzytkownika(id: number){

    let headers = new Headers();
    let token = JSON.parse(localStorage.getItem('auth_token'));
    headers.append('Content-Type', "application/json");
    headers.append('Authorization', token.token);

    return this.http.post(this.URL,id,{headers})
      .map(res =>{
        this.tablicaWybor=res.json();
        return this.tablicaWybor;

      });

  }
  getWybor(){
    return this.wybor;
  }
  setWybor(wybor){
    this.wybor=wybor;
  }
  getTablicaWybor(){
    return this.tablicaWybor;
  }
  getWyborZTablica(){
   for(let n=0;n <this.tablicaWybor.length;n++){
     if(this.tablicaWybor[n].ID==this.wybor){
       return this.tablicaWybor[n];
     }
   }
  }
}
