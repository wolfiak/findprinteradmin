import { Injectable } from '@angular/core';
import { IUzytkownik } from './../model/IUzytkownik';
import { environment } from './../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
@Injectable()
export class LogowanieService {
  private loggedIn: boolean = false;
  private URL: string = environment.URL + 'api/uzytkownik/login';
  private URL2: string = environment.URL + 'api/uzytkownik/zalogowany';
  private URL3: string = environment.URL + 'api/uzytkownik/getOne';

  constructor(private http: Http) { }


  loguj(passy: IUzytkownik){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.URL, JSON.stringify(passy), { headers })
        .map(res => {
            let reso = res.json();
            let tok = {
                token: 'Bearer '+reso.acess_token,
                id: reso.id
            };
            console.log("Logowanie poszlo");
            console.log(res);
            localStorage.setItem('auth_token', JSON.stringify(tok));
            this.loggedIn = true;
            return true;
        });
  }
  isLoggedIn() {
    if(localStorage.getItem('auth_token')){
      let headers = new Headers();
      let token = JSON.parse(localStorage.getItem('auth_token'));
      headers.append('Content-Type', "application/json");
      headers.append('Authorization', token.token);
      return this.http.get(this.URL2, { headers });
    }else{
        console.log("Nie ma tokenu!");
     return  Observable.create(sub =>{
        sub.error(new Error("Toke nie istnieje"));
     });

    }
    
}
logout() {
    localStorage.removeItem('auth_token');
    this.loggedIn = false;
}
getUz(id){
    let headers = new Headers();
    headers.append('Content-Type', "application/json");

    return this.http.get(this.URL3+`?id=${id}`,{ headers })
        .map(res=>{
            console.log("Uzytkownik:");
            console.log(res)
            
            return res.json();

        });
}
}
