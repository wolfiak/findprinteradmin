import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { of } from 'rxjs/observable/of';
@Injectable()
export class ModalService {
  private modal2: boolean=false;
  private modalChange: Subject<any>=new Subject<any>();
  private tmpDoModala:any;
  constructor() { }

  setTmpDoModala(tmp){
    this.tmpDoModala=tmp;
  }
  getTmpDoModal(){
    return this.tmpDoModala;
  }
  getModal2(){
      
   
     return this.modalChange;
  }
  setModal2(){
    if(this.modal2){
      this.modal2=false;
    }else{
      this.modal2=true;
    }
    this.modalChange.next(this.modal2);
  }
  getKolor(kolor){
    if(kolor == "Na jutro"){
      return "#43a047";
    }else if(kolor == "Na dziś"){
      return "#fdd835";
    }else if(kolor == "Na zaraz"){
      return "#f4511e";
    }else if(kolor == "Opóźniony"){
      return "#e53935";
    }
  }

}
