import { FindPrinterAdminPage } from './app.po';

describe('find-printer-admin App', () => {
  let page: FindPrinterAdminPage;

  beforeEach(() => {
    page = new FindPrinterAdminPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
